interface Row {
    column: number
    items: Forms[]
}


export interface Forms {
    label?: string,
    name: string,
    placeholder?: any,
    row?: Row,
    form_type?: 'text' | 'password',
    type?: "Select" | "Text" | "Radio" | "CheckBox" | "File" | "TextArea",
    options?: any[],
    disabled?: boolean,
    bool?: boolean
}


