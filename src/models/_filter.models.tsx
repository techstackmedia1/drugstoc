import { optionsModel } from "./_options.models"

export interface filterModel {
    name: string,
    label: string,
    icon: JSX.Element,
    type?: 'CheckBox' | 'Range' | 'Radio'
    searchable?: boolean,
    path?: string,
    data?: Array<optionsModel>
    range?: number
    apply?: any
}