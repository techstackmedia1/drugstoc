import { PRODUCT_CATEGORY } from "../constants/_api.constant";
import { useConnect } from "../utilities/connectHooks";
// import productCategory from './sidebarData'
const ServerData = () => {
  const { dataCategory } = useConnect({ path: PRODUCT_CATEGORY });
  console.log(dataCategory.results);
  return <div>serverData</div>;
};

export default ServerData;
