import { Box, Divider, Container, Stack, Grid } from "@mui/material";
import { filterList } from "../constants/_filter.constant";
import FilterChip from "../element/filterChip";
import "./productListLayout.css";
import { useConnect } from "../utilities/connectHooks";
import { useState } from "react";
import Layout from "./Layout.component";
import { Link, useLocation } from "react-router-dom";
import Pagination from "@mui/material/Pagination";
import PaginationItem from "@mui/material/PaginationItem";
import { Content } from "../styles";
import Products from "../components/Products.component";
import {
  PRODUCT_BY_CATEGORY,
  PRODUCT_CATEGORY,
  PRODUCT_MANUFACTURERS,
  RANDOM_PRODUCT,
} from "../constants/_api.constant";
import "../pages/Home.page.css";
interface Props {
  children: JSX.Element;
  setParams: React.Dispatch<React.SetStateAction<string>>;
}

interface Data {
  label: string;
  value: string;
}

function ProductListLayout({ children, setParams }: Props) {
  const [filter, setFilter] = useState<Data[]>([]);
  const location = useLocation();
  const path = location.pathname.split("/");
  const slug = path[path.length - 1];
  const title = slug.replaceAll("-", " ");

  const t = title.split(" ");
  const x = t.map((item: any) => {
    const a: any = item[0].toUpperCase();
    const b: any = item.slice(1);
    return `${a}${b}`;
  });

  console.log(x.join(" "));

  const query = new URLSearchParams(location.search);
  const page = parseInt(query.get("page") || "1", 10);
  const { data, isFetching, hasError } = useConnect({
    path: `https://staging.drugstoc.com/api/v2/inventory/products?category=&manufacturer`,
  });

  const apply = (data: any) => {
    if (typeof data !== "string") {
      let rd = arrayUnique(filter, data);
      let resp = rd.map((element) => `&${element.label}=${element.value}`);
      setParams("?category=" + resp.join(""));
      setFilter(rd);
    }
  };

  const { data: category } = useConnect({
    path: `https://staging.drugstoc.com/api/v2/inventory/products?category=&manufacturer`,
  });
  console.log(location.pathname, category)

  const { data: productManufacturer } = useConnect({
    path: PRODUCT_MANUFACTURERS,
  });
  // console.log(productManufacturer);

  const { data: random } = useConnect({ path: RANDOM_PRODUCT });
  const slugs = random?.results.map((item: any) => {
    return item.slug;
  });

  const { data: ppp } = useConnect({ path: PRODUCT_CATEGORY });
  // console.log(ppp);

  const params = random?.results.filter((item: any) => {
    return item.manufacturer !== null && item.category !== null;
  });
  // console.log(params);

  /* */

  return (
    <Layout>
      <div id="productListLayout">
        <Box>
          <Box>
            <Box
              component="img"
              sx={{
                width: "100%",
                height: 150,
                marginTop: 3,
                marginBottom: 2,
                objectFit: "cover",
              }}
              alt="ads image"
              src="/no_image.png"
            />
            <Stack
              direction="row"
              mt={2}
              spacing="21px"
              mb={2}
              sx={{ display: { xs: "none", md: "flex" } }}
            >
              {filterList.map((element, index) => (
                <FilterChip key={index} {...element} apply={apply} />
              ))}
            </Stack>
            <Divider />
          </Box>

          <div className="productListLayout">
            <hr />
            <div>
              <h2>{x.join(" ")}</h2>
              <p>
                All products listed are categorized under {x.join(" ")}{" "}
                medication.
              </p>
            </div>
            <hr />
            <div>
              <p className="result">result: {category?.count} products</p>
            </div>

            <div id="home">
              <Box sx={{ marginTop: 3 }}>
                <Container fixed>
                  <Divider />
                  {!isFetching ? (
                    <Grid sx={{ marginTop: 1 }} container spacing={3}>
                      {data?.results
                        .sort((a: any, b: any) => b?.quantity - a?.quantity)
                        .map((element: any, index: number) => (
                          <Grid key={index} item xs={6} md={3}>
                            <Products
                              id={element?.id}
                              title={element?.name}
                              image={element?.image}
                              description={element?.desc}
                              price={element?.price}
                              slug={element?.slug}
                              trigger={RANDOM_PRODUCT}
                              quantity_in_cart={element?.quantity_in_cart}
                              in_cart={element?.in_cart}
                              quantity={element?.quantity}
                              is_favorite={element?.is_favorite}
                            />
                          </Grid>
                        ))}
                    </Grid>
                  ) : hasError ? null : (
                    <Content>Loading.....</Content>
                  )}
                </Container>
              </Box>
            </div>

            {Math.ceil(category?.count / category?.results.length) ===
            1 ? null : (
              <Pagination
                page={page}
                count={Math.ceil(category?.count / category?.results.length)}
                renderItem={(item) => (
                  <PaginationItem
                    component={Link}
                    to={`/category/steroid${
                      item.page === 1 ? "" : `?page=${item.page}`
                    }`}
                    {...item}
                  />
                )}
                onClick={() => window.location.reload()}
              />
            )}
          </div>
        </Box>
      </div>
    </Layout>
  );
}

export default ProductListLayout;

function arrayUnique(array: Data[], data: any) {
  let a: any[] = [];
  if (data.length > 0) {
    a = array;
    let index = array.filter(
      (ar) => !data.find((rm: any) => rm.label === ar.label)
    );
    a = index.concat(data);
  } else {
    a = array;
    let index = a.filter((item) => item.label !== data.label);
    a = index.concat(data);
  }

  return a;
} 
