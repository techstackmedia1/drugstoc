import { useCallback, useEffect, /*useRef,*/ useState } from "react";
import "./Carousel.component.css";
import { MdArrowBackIos } from "react-icons/md";
import { MdArrowForwardIos } from "react-icons/md";
// import { useIntersection } from "./IntersectionObserver.component";

const photos = [
  {
    id: "p1",
    title: "Personal Protective Overall Banner",
    url: "https://res.cloudinary.com/bizstak/image/upload/v1661622298/image3_lda1ei.png",
  },
  {
    id: "p2",
    title: "Business Expand with Drugstoc",
    url: "https://res.cloudinary.com/bizstak/image/upload/v1661622308/image2_emlri1.png",
  },
];

function App() {
  const [currentIndex, setCurrentIndex] = useState(0);

  const next = useCallback(() => {
    setCurrentIndex((currentIndex + 1) % photos.length);
  }, [currentIndex]);

  const prev = () => {
    setCurrentIndex((currentIndex - 1 + photos.length) % photos.length);
  };

  useEffect(() => {
    const handleAutoplay = setInterval(next, 5000);
    return () => clearInterval(handleAutoplay);
  }, [next]);

  return (
    <>
      <div className="slider-container">
        {photos.map((photo) => (
          <div
            key={photo.id}
            className={
              photos[currentIndex].id === photo.id ? "fade" : "slide fade"
            }
          >
            {/* {isInView && ( */}
            <img src={photo.url} alt={photo.title} className="photo" />
            {/* )} */}
            {/* <img src={photo.url} alt={photo.title} className="photo" /> */}
            {/* <div className="caption">{photo.title}</div> */}
          </div>
        ))}

        <button onClick={prev} className="prev">
          <MdArrowBackIos />
        </button>

        <button onClick={next} className="next">
          <MdArrowForwardIos />
        </button>
      </div>

      <div className="dots">
        {photos.map((photo) => (
          <span
            key={photo.id}
            className={
              photos[currentIndex].id === photo.id ? "dot active" : "dot"
            }
            onClick={() => setCurrentIndex(photos.indexOf(photo))}
          ></span>
        ))}
      </div>
    </>
  );
}

export default App;
