import * as React from "react";
import { AppBar, Box, Toolbar, IconButton, Badge, Menu } from "@mui/material/";
import MenuIcon from "@mui/icons-material/Menu";
import { Content } from "../styles";
import {
  Button,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  Stack,
} from "@mui/material";
import { Link } from "react-router-dom";
import { Cart, Category, Manufacturers, User } from "../element/icons";
import ElevationScroll from "../element/elevation";
import SearchBar from "./SearchBar.component";
import { CART_ITEMS, PRODUCT_CATEGORY } from "../constants/_api.constant";
import { useConnect } from "../utilities/connectHooks";
import { MdOutlineCancel } from "react-icons/md";
import Side from "./Side";
import "./Navbar.component.css";

interface Props {
  window?: () => Window;
}

const drawerWidth = 240;
const navItems = [
  { name: "Manufacturer", path: "/manufacturer" },
  { name: "Category", path: "/category" },
  { name: "Account", path: "/account" },
  { name: "Cart", path: "/cart" },
  { name: "Purchase History", path: "/account/purchase_history" },
  { name: "DrugStoc Pay", path: "/account/wallet" },
  { name: "Personal Information", path: "/account/personal_info" },
  { name: "Business Information", path: "/account/business" },
  { name: "Shipping Addresses", path: "/account/shipping_address" },
  { name: "My Favorite", path: "/account/favorite" },
  { name: "Change Password", path: "/account/change-password" },
  { name: "Help", path: "/help" },
  { name: "Term of use", path: "/term" },
];

function Navbar(props: Props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const { data, isFetching, hasError } = useConnect({ path: PRODUCT_CATEGORY });
  const { data: cartItem } = useConnect({ path: CART_ITEMS });
  const { data: name } = useConnect({ path: PRODUCT_CATEGORY });

  const Options = ({ name, href }: any) => (
    <ListItem disablePadding>
      <ListItemButton component="a" href={`/category/${href}`}>
        <Content px={2} py={0.5} sx={{ fontSize: 12 }}>
          {name}
        </Content>
      </ListItemButton>
    </ListItem>
  );

  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <div id="sidebar">
      <Box sx={{ flexGrow: 1 }}>
        <ElevationScroll {...props}>
          <AppBar
            sx={{
              paddingLeft: { xs: 0, md: 10 },
              paddingRight: { xs: 0, md: 10 },
              position: "fixed",
            }}
          >
            <Toolbar>
              <Link to="/">
                <Box
                  component="img"
                  sx={{
                    width: 160,
                    display: { xs: "none", sm: "block" },
                    maxHeight: { xs: 233, md: 167 },
                    maxWidth: { xs: 350, md: 250 },
                  }}
                  alt="The house from the offer."
                  src="/logo_white.svg"
                />
                <Box
                  component="img"
                  sx={{
                    width: 50,
                    marginRight: 1,
                    display: { md: "none", sm: "block" },
                    maxHeight: { xs: 35, md: 150 },
                    maxWidth: { xs: 35, md: 150 },
                  }}
                  alt="The house from the offer."
                  src="/logo_white_mobile.svg"
                />
              </Link>
              <Box sx={{ flexGrow: 1 }} />
              <Box sx={{ display: { xs: "none", md: "flex" } }}>
                <Stack direction="row" spacing={3}>
                  <Button
                    onClick={handleClick}
                    size="small"
                    aria-controls={open ? "account-menu" : undefined}
                    aria-haspopup="true"
                    aria-expanded={open ? "true" : undefined}
                    sx={{ my: 2, color: "white", textTransform: "none" }}
                    startIcon={<Category />}
                  >
                    Category
                  </Button>
                  <Menu
                    anchorEl={anchorEl}
                    id="account-menu"
                    open={open}
                    onClose={handleClose}
                    onClick={handleClose}
                    PaperProps={{
                      elevation: 0,
                      sx: {
                        overflow: "visible",
                        filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
                        mt: 1.5,
                        "& .MuiAvatar-root": {
                          width: 32,
                          height: 32,
                          ml: -0.5,
                          mr: 1,
                        },
                        "&:before": {
                          content: '""',
                          display: "block",
                          position: "absolute",
                          top: 0,
                          left: "48%",
                          width: 10,
                          height: 10,
                          bgcolor: "background.paper",
                          transform: "translateY(-50%) rotate(45deg)",
                          zIndex: 0,
                        },
                      },
                    }}
                    transformOrigin={{ horizontal: "center", vertical: "top" }}
                    anchorOrigin={{ horizontal: "center", vertical: "bottom" }}
                  >
                    <Box
                      sx={{
                        width: "300px",
                        maxWidth: 360,
                        maxHeight: 400,
                        overflow: "scroll",
                        overflowX: "hidden",
                        bgcolor: "background.paper",
                      }}
                    >
                      {!isFetching && hasError === undefined ? (
                        <List dense={true}>
                          <ListItem disablePadding>
                            <Content
                              textAlign="center"
                              px={2}
                              py={1}
                              sx={{ fontSize: 14, fontWeight: "700" }}
                            >
                              All Categories
                            </Content>
                          </ListItem>
                          {data.results.map((element: any, index: number) => (
                            <Options
                              key={index}
                              name={element.name}
                              href={`${element.slug}`}
                            />
                          ))}
                        </List>
                      ) : null}
                    </Box>
                  </Menu>
                  <Button
                    href="/manufacturer"
                    sx={{
                      my: 2,
                      color: "white",
                      textTransform: "none",
                      fontWeight: 500,
                    }}
                    startIcon={<Manufacturers />}
                  >
                    Manufacturer
                  </Button>
                </Stack>
              </Box>
              <Box sx={{ flexGrow: 1 }} />
              <SearchBar />
              <Box sx={{ flexGrow: 5 }} />
              <Box sx={{ display: { xs: "none", md: "flex" } }}>
                <Stack direction="row" spacing={3}>
                  <Button
                    href="/account"
                    sx={{ my: 2, color: "white", textTransform: "none" }}
                    startIcon={<User />}
                  >
                    Account
                  </Button>

                  {/* ------------- */}
                  <Button
                    href="/cart"
                    sx={{ my: 2, color: "white", textTransform: "none" }}
                    startIcon={
                      <Badge
                        badgeContent={cartItem?.results.length}
                        color="error"
                      >
                        <Cart />
                      </Badge>
                    }
                  >
                    Cart
                  </Button>
                  {/* -------------------- */}
                </Stack>
              </Box>
              <Box sx={{ display: { xs: "flex", md: "none" } }}>
                <IconButton
                  size="large"
                  color="inherit"
                  aria-label="open drawer"
                  edge="end"
                  onClick={handleDrawerToggle}
                >
                  <MenuIcon />
                </IconButton>
              </Box>
              <Box component="nav">
                <Drawer
                  container={container}
                  variant="temporary"
                  open={mobileOpen}
                  onClose={handleDrawerToggle}
                  ModalProps={{
                    keepMounted: true, // Better open performance on mobile.
                  }}
                  sx={{
                    display: { xs: "block", sm: "none" },
                    "& .MuiDrawer-paper": {
                      boxSizing: "border-box",
                      width: drawerWidth,
                    },
                  }}
                >
                  <Side />
                </Drawer>
              </Box>
            </Toolbar>
          </AppBar>
        </ElevationScroll>
        <Toolbar />
      </Box>
    </div>
  );
}

export default Navbar;
