import { Add, FavoriteBorder, Remove } from "@mui/icons-material";
import { Box, Divider, Stack } from "@mui/material";
import { FC, useState } from "react";
import { Link } from "react-router-dom";
import { CART_ITEMS, FAVORITE_PRODUCT } from "../constants/_api.constant";
import { Delete, Favorite } from "../element/icons";
import {
  AddToCartButton,
  Content,
  ProductsContainer,
  Title,
  IconButton,
} from "../styles";
import { request } from "../utilities/connectHooks";
import { priceFormat } from "../utilities/priceFormatter";

interface Props {
  quantity_in_cart?: any;
  is_favorite?: boolean;
  in_cart?: any;
  id?: any;
  trigger: string;
  addItemToCart?: (arg0: { quantity: number; product_id: any }) => Promise<any>;
  loadCartItem?: () => void;
  addItemToFav?: (arg0: { product_id: any }) => Promise<any>;
  deleteItemToFav?: (arg0: any) => Promise<any>;
  updateItemInCart?: (
    arg0: { quantity: any; product_id: any },
    arg1: any
  ) => Promise<any>;
  deleteItemInCart?: (arg0: any) => Promise<any>;
  slug?: any;
  image?: any;
  title?: any;
  price?: any;
  description?: any;
  quantity?: number;
  in_fav?: boolean;
}

const Products: FC<Props> = (props: Props) => {
  let [quantity, setQuantity] = useState<number>(props.quantity_in_cart || 1);
  const [loading_decrease, setLoadingDecrease] = useState(false);
  const [loading_increase, setLoadingIncrease] = useState(false);
  const [inFav, setFav] = useState(props.is_favorite);
  const [inCart, setCart] = useState(props.in_cart);
  const [loading_cart, setLoadingCart] = useState(false);
  const [loading_fav, setLoadingFav] = useState(false);
  const [loading_del, setLoadingDelete] = useState(false);

  function add_to_cart() {
    let data = {
      quantity: 1,
      product_id: props.id,
    };
    console.log(data);
    setLoadingCart(true);
    request.post({path: CART_ITEMS, trigger: CART_ITEMS, payload: data}).then(res => {
        if (res) {
            setLoadingCart(false);
            setLoadingCart(false)
            setCart(true)
        } else {
            setLoadingCart(false)
        }
    })
  }

  function add_to_fav() {
    let data = {
      product_id: props.id,
    };
    setLoadingFav(true);
    request
      .post({ path: FAVORITE_PRODUCT, trigger: "", payload: data })
      .then(() => {
        setFav(true)
        setLoadingFav(false)
    });
  }

  function delete_fav() {
    setLoadingDelete(true);
    request.delete({path: `${FAVORITE_PRODUCT}/${props.id}`, trigger: FAVORITE_PRODUCT}).then(() => {
        setLoadingDelete(false)
    })
  }

  function increase_item() {
    setLoadingIncrease(true);
    let data = {
      quantity: quantity + 1,
      product_id: props.id,
    };
    request.patch({path: `${CART_ITEMS}/${props.id}`, trigger: CART_ITEMS, payload: data}).then(resp => {
        if (resp) {
            setQuantity(quantity += 1)
        } 
        setLoadingIncrease(false)
    })
  }

  function decrease_item() {
    setLoadingDecrease(true);
    let data = {
      quantity: quantity - 1,
      product_id: props.id,
    };
    if (quantity === 1) {
        request.delete({path: `${CART_ITEMS}/${props.id}`, trigger: CART_ITEMS}).then(resp => {
            if (resp) {
                setQuantity(1)
                setLoadingDecrease(false)
                setLoadingCart(false);
                setCart(false);
            }
            setLoadingDecrease(false)
        })
    } else {
        request.patch({path: `${CART_ITEMS}/${props.id}`, trigger: CART_ITEMS, payload: data}).then(resp => {
            if (resp) {
                setQuantity(quantity -= 1)
            } 
            setLoadingDecrease(false)
        })
    }
  }

  return (
    <ProductsContainer variant="outlined">
      <Link to={`/products/${props.slug}`}>
        <Box
          component="img"
          loading="lazy"
          sx={{
            width: "100%",
            height: 230,
            cursor: "pointer",
            objectFit: "contain",
          }}
          alt="product image"
          src={props.image}
        />
      </Link>
      <Box sx={{ padding: 2 }}>
        <Content
          sx={{
            fontSize: 12,
            textOverflow: "ellipsis",
            overflow: "hidden",
            color: "#000",
            whiteSpace: "nowrap",
          }}
        >
          {props.title}
        </Content>
        <Divider sx={{ marginTop: 0.5 }} />
        <Title
          variant="h6"
          sx={{
            filter: props.price ? "blur(0px)" : "blur(5px)",
          }}
        >
          {props.price ? priceFormat(props.price) : "PRICE NOT AVAILABLE"}
        </Title>
        {/* <Divider sx={{ marginBottom: 0.5 }} /> */}
        <Content
          sx={{
            fontSize: 10,
            textOverflow: "ellipsis",
            overflow: "hidden",
            height: 30,
          }}
        >
          {props.description}
        </Content>
        
        <Stack direction="row" sx={{ marginTop: 2, height: 40 }} spacing={1}>
          {!inCart ? (
            <AddToCartButton
              variant="contained"
              disabled={props.quantity === 0}
              onClick={add_to_cart}
              loading={loading_cart}
              sx={{ width: "100%", fontSize: 12, fontWeight: "900" }}
              disableElevation
            >
              {props.quantity === 0 ? "Out of Stock" : "Add to cart"}
            </AddToCartButton>
          ) : (
            <Stack
              direction="row"
              justifyContent="space-between"
              width="100%"
              spacing={3}
            >
              <IconButton
                onClick={decrease_item}
                loading={loading_decrease}
                variant="contained"
              >
                <Remove />
              </IconButton>
              <Title>{quantity}</Title>
              <IconButton
                onClick={increase_item}
                loading={loading_increase}
                variant="contained"
              >
                <Add />
              </IconButton>
            </Stack>
          )}
          {!props.in_fav ? (
            <IconButton
              onClick={add_to_fav}
              loading={loading_fav}
              disabled={props.in_fav || inFav}
              variant="outlined"
            >
              {props.is_favorite || inFav ? <Favorite /> : <FavoriteBorder />}
            </IconButton>
          ) : (
            <IconButton
              onClick={delete_fav}
              loading={loading_del}
              color="error"
              variant="outlined"
            >
              <Delete />
            </IconButton>
          )}
        </Stack>
      </Box>
    </ProductsContainer>
  );
};

export default Products;