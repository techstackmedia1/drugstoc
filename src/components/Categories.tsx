import { Box, Divider, Stack } from "@mui/material";
import { filterList } from "../constants/_filter.constant";
import FilterChip from "../element/filterChip";
import "./productListLayout.css";
import { MdOutlineFavoriteBorder } from "react-icons/md";
import { useConnect } from "../utilities/connectHooks";
import { useState } from "react";
import Layout from "./Layout.component";
import { Link, useLocation } from "react-router-dom";
import Pagination from "@mui/material/Pagination";
import PaginationItem from "@mui/material/PaginationItem";
import { PRODUCT_CATEGORY } from "../constants/_api.constant";

interface Props {
  children: JSX.Element;
  setParams: React.Dispatch<React.SetStateAction<string>>;
}

interface Data {
  label: string;
  value: string;
}

function ProductListLayout({ children, setParams }: Props) {
  const [filter, setFilter] = useState<Data[]>([]);
  const location = useLocation();
  const query = new URLSearchParams(location.search);
  const page = parseInt(query.get("page") || "1", 10);

  const apply = (data: any) => {
    if (typeof data !== "string") {
      let rd = arrayUnique(filter, data);
      let resp = rd.map((element) => `&${element.label}=${element.value}`);
      setParams("?category=" + resp.join(""));
      setFilter(rd);
    }
  };
  const params = location.pathname;
  const len = params.split("/");
  const product = len[len.length - 1];

  const { data: d } = useConnect({
    path: `https://staging.drugstoc.com/api/v2/inventory/products/${product}`,
  });
  console.log(d);

  const { data: log } = useConnect({
    path: `https://staging.drugstoc.com/api/v2/inventory/products?category=&manufacturer`,
  });

  const { data: pro } = useConnect({ path: PRODUCT_CATEGORY });
  const categories = pro?.results.map((item: any) => {
    return item.name;
  });
  const array = log?.results.filter((item: any) => {
    let element = "";
    for (let index = 0; index < categories.length; index++) {
      element = categories[index];
    }
    return item.category === element;
  });

  const VitaminsAminoacidsSupplementsA = log?.results.filter((item: any) => {
    return item.category === "Vitamins,Amino acids & SupplementsA";
  });
  const ProtectiveDisinfectingDevices = log?.results.filter((item: any) => {
    return item.category === "Protective & Disinfecting Devices";
  });
  const ProtectionMaterials = log?.results.filter((item: any) => {
    return item.category === "Protection Materials";
  });
  const cough = log?.results.filter((item: any) => {
    return item.category === "cough";
  });
  const Vaccines = log?.results.filter((item: any) => {
    return item.category === "Vaccines";
  });
  const VitaminsAminoacidsSupplements = log?.results.filter((item: any) => {
    return item.category === "Vitamins,Amino acids & Supplements";
  });
  const sexualHealth = log?.results.filter((item: any) => {
    return item.category === "Sexual Health";
  });
  const RapidDiagnosticTestDevices = log?.results.filter((item: any) => {
    return item.category === "Rapid Diagnostic Test Devices";
  });
  const Oncology = log?.results.filter((item: any) => {
    return item.category === "Oncology";
  });
  console.log(Oncology);
  const MedicalDevicesConsumables = log?.results.filter((item: any) => {
    return item.category === "Medical Devices and Consumables";
  });
  const Household = log?.results.filter((item: any) => {
    return item.category === "Household";
  });
  const GIT = log?.results.filter((item: any) => {
    return item.category === "GIT";
  });
  const FluidElectrolyteBalance = log?.results.filter((item: any) => {
    return item.category === "Fluid & Electrolyte Balance";
  });
  const EndocrineSystem = log?.results.filter((item: any) => {
    return item.category === "Endocrine System";
  });
  const ENTOpthalmic = log?.results.filter((item: any) => {
    return item.category === "ENT & Opthalmic";
  });
  const CoughColdRespiratorySystem = log?.results.filter((item: any) => {
    return item.category === "Cough,Cold & Respiratory System";
  });
  console.log(CoughColdRespiratorySystem);

  const AntiVirals = log?.results.filter((item: any) => {
    return item.category === "Anti-Virals";
  });
  const AntiSepticsDisinfectants = log?.results.filter((item: any) => {
    return item.category === "Anti-Septics & Disinfectants";
  });
  const AntiMalarial = log?.results.filter((item: any) => {
    return item.category === "Anti-Malarial";
  });
  const AntiHypertensivesCardiovascular = log?.results.filter((item: any) => {
    return item.category === "Anti-Hypertensives & Cardiovascular";
  });
  const AntiHistamines = log?.results.filter((item: any) => {
    return item.category === "Anti-Histamines";
  });
  const AntiFungals = log?.results.filter((item: any) => {
    return item.category === "Anti-Fungals";
  });
  const AntiBiotics = log?.results.filter((item: any) => {
    return item.category === "Anti-Biotics";
  });
  const Analgesics = log?.results.filter((item: any) => {
    return item.category === "Analgesics";
  });

  const drugStocCategory = [
    VitaminsAminoacidsSupplementsA,
    ProtectiveDisinfectingDevices,
    ProtectionMaterials,
    cough,
    Vaccines,
    VitaminsAminoacidsSupplements,
    sexualHealth,
    RapidDiagnosticTestDevices,
    Oncology,
    MedicalDevicesConsumables,
    Household,
    GIT,
    FluidElectrolyteBalance,
    EndocrineSystem,
    ENTOpthalmic,
    AntiVirals,
    AntiSepticsDisinfectants,
    AntiMalarial,
    AntiHypertensivesCardiovascular,
    AntiHistamines,
    AntiFungals,
    AntiBiotics,
    Analgesics,
  ];

  const mapItems = drugStocCategory
    .map((item) => {
      return item[0];
    })
    .filter((item) => {
      return item != undefined;
    });
  console.log(mapItems);

  const { data: category } = useConnect({
    path: `https://staging.drugstoc.com/api/v2/inventory/products?category=&manufacturer`,
  });

  return (
    <Layout>
      <div id="productListLayout">
        <Box>
          <Box>
            <Box
              component="img"
              sx={{
                width: "100%",
                height: 150,
                marginTop: 3,
                marginBottom: 2,
                objectFit: "cover",
              }}
              alt="ads image"
              src="/no_image.png"
            />
            <Stack
              direction="row"
              mt={2}
              spacing="21px"
              mb={2}
              sx={{ display: { xs: "none", md: "flex" } }}
            >
              {filterList.map((element, index) => (
                <FilterChip key={index} {...element} apply={apply} />
              ))}
            </Stack>
            <Divider />
          </Box>

          <div className="productListLayout">
            <hr />
            <div>
              <h2>Anti Malaria</h2>
              <p>
                All products listed are categorized under Anti- Malaria
                medication.
              </p>
            </div>
            <hr />
            <div>
              <p className="result">result: {category?.count} products</p>
            </div>

            <div className="cardGroup">
              {}
            </div>

            {Math.ceil(category?.count / category?.results.length) ===
            1 ? null : (
              <Pagination
                page={page}
                count={Math.ceil(category?.count / category?.results.length)}
                renderItem={(item) => (
                  <PaginationItem
                    component={Link}
                    to={`/category/steroid${
                      item.page === 1 ? "" : `?page=${item.page}`
                    }`}
                    {...item}
                  />
                )}
                onClick={() => window.location.reload()}
              />
            )}
          </div>
        </Box>
      </div>
    </Layout>
  );
}

export default ProductListLayout;

function arrayUnique(array: Data[], data: any) {
  let a: any[] = [];
  if (data.length > 0) {
    a = array;
    let index = array.filter(
      (ar) => !data.find((rm: any) => rm.label === ar.label)
    );
    a = index.concat(data);
  } else {
    a = array;
    let index = a.filter((item) => item.label !== data.label);
    a = index.concat(data);
  }

  return a;
}

// export default function PaginationLink() {
//   return (
//     <MemoryRouter initialEntries={['/inbox']} initialIndex={0}>
//       <Routes>
//         <Route path="*" element={<ProductListLayout />} />
//       </Routes>
//     </MemoryRouter>
//   );
// }
