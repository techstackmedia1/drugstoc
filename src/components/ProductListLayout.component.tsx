import { Box, Divider, Stack } from "@mui/material";
import { useState } from "react";
import { filterList } from "../constants/_filter.constant";
import FilterChip from "../element/filterChip";

interface Props {
  children: JSX.Element;
  setParams: React.Dispatch<React.SetStateAction<string>>;
}

interface Data {
  label: string;
  value: string;
}

function ProductListLayout({ children, setParams }: Props) {
  const [filter, setFilter] = useState<Data[]>([]);

  const apply = (data: any) => {
    if (typeof data !== "string") {
      let rd = arrayUnique(filter, data)
      let resp = rd.map(element => `&${element.label}=${element.value}`)
      setParams("?category="+resp.join(''))
      setFilter(rd);
    }
  };

  return (
    <Box>
      <Box mt={4} mb={2}>
      <Box
          component="img"
          sx={{
            width: "100%",
            height: 150,
            marginTop: 3,
            marginBottom: 2,
            objectFit: "cover",
          }}
          alt="ads image"
          src="/no_image.png"
        />
        <Stack direction="row" mt={2} spacing="21px" mb={2} sx={{ display: { xs: 'none', md: 'flex' } }}>
          {filterList.map((element, index) => (
            <FilterChip key={index} {...element} apply={apply} />
          ))}
        </Stack>
        <Divider />
      </Box>
      {children}
    </Box>
  );
}

export default ProductListLayout;


function arrayUnique(array: Data[], data:any) {
  let a: any[] = []
  if (data.length  > 0) {
    a = array
    let index = array.filter(ar => !data.find((rm: any) => (rm.label === ar.label)))
    a = index.concat(data)
  } else {
    a = array
    let index = a.filter(item => item.label !== data.label);
    a = index.concat(data)
  }

  return a;
}
