import { Box, Divider, Stack } from "@mui/material";
import FilterChip from "../../element/filterChip";
import { filterList } from "../../constants/_filter.constant";
import productListLayout from "./productListLayout.module.css";
import { MdOutlineFavoriteBorder } from "react-icons/md";
import { useEffect, useState } from "react";
import axios from "axios";
import { useConnect } from "../../utilities/connectHooks";
import { PRODUCT_DETAIL } from "../../constants/_api.constant";
import { isTemplateExpression } from "typescript";

interface Props {
  children: JSX.Element;
  setParams: React.Dispatch<React.SetStateAction<string>>;
}

interface Data {
  label: string;
  value: string;
}

function ProductListLayout({ children, setParams }: Props, item: any) {
  return (
    <div id="productID">
      <div className={productListLayout.productListLayout}>
        <hr />
        <div>
          <h2>Anti Malaria</h2>
          <p>
            All products listed are categorized under Anti- Malaria medication.
          </p>
        </div>
        <hr />
        <div>
          <p className={productListLayout.result}>result: 1000 products</p>
        </div>

        <div className={productListLayout.cardGroup}>
          <div className={productListLayout.card}>
            <img src={item.image} alt="img" />
            <div>
              <h4>{item.name}</h4>
            </div>
            <div>
              <hr />
              <p>{item.desc}</p>
              <hr />
            </div>
            <div className={productListLayout.button}>
              <button>Add to Cart</button>
              <button>
                <MdOutlineFavoriteBorder size={28} />
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProductListLayout;

/* 
const simpleArrayJson = '["Java", "Python", "JavaScript"]';
const objectArrayJson = '[{"name": "Java", "description": "Java is a class-based, object-oriented programming language."},{"name": "Python", "description": "Python is an interpreted, high-level and general-purpose programming language."}, {"name": "JS", "description": "JS is a programming language that conforms to the ECMAScript specification."}]';

const simpleArray = JSON.parse(simpleArrayJson);
const objectArray = JSON.parse(objectArrayJson);

console.log(simpleArray);
console.log(objectArray);
*/

// const [state, setState] = useState([]);
// const { data } = useConnect({
//   path: "https://staging.drugstoc.com/api/v2/inventory/products?manufacturer=2261",
// });

// useEffect(() => {
//   const fetchAPI = async () => {
//     const response: any = await axios(data, {
//       headers: {
//         Accept: "application/json",
//       },
//     });

//     const res: any = response.config.results;
//     setState(res);
//   };
//   fetchAPI();
// });
