import { Button, Typography, Box, Badge } from "@mui/material";
import Modal from "@mui/material/Modal";
// import { Button, Typography, Modal,Box, Link } from "@mui/material";
import { MdKeyboardArrowDown } from "react-icons/md";

import { useState } from "react";
import { CART_ITEMS } from "../constants/_api.constant";
import { Cart, Menu } from "../element/icons";
import { useConnect } from "../utilities/connectHooks";

export default function SidebarItem({ item }) {
  const { data: cartItem } = useConnect({ path: CART_ITEMS });
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const logout = () => {
    localStorage.removeItem("token");
  };

  const styles = {
    color: "#1F1C1",
  };
  const [open, setOpen] = useState(false);

  if (item.childrens) {
    return (
      <div className={open ? "sidebar-item open" : "sidebar-item"}>
        <div className="sidebar-name">
          <span>
            {item.icon && <i style={styles}></i>}
            {item.name}
          </span>
          <i
            className="bi-chevron-down toggle-btn"
            onClick={() => setOpen(!open)}
          ></i>
        </div>
        <div className="sidebar-content">
          {item.childrens.map((child, index) => (
            <SidebarItem key={index} item={child} />
          ))}
        </div>
      </div>
    );
  } else {
    return (
      <>
        <a href={`${item.slug}` || "#"} className="sidebar-item plain">
          {item.icon && <i style={styles}></i>}
          {item.name}
        </a>

        {item.name === "Cart" ? (
          <Badge badgeContent={cartItem?.results.length} color="error"></Badge>
        ) : null}

        {/* {item.name === "Log Out" ? (
          <div style={{ width: 350, border: "none" }}>
            <Button onClick={handleOpen}>Sign Out</Button>
            <Modal
              open={open}
              onClose={handleClose}
              aria-labelledby="modal-modal-title"
              aria-describedby="modal-modal-description"
            >
              <Box>
                <div style={{ textAlign: "center" }}>
                  <Typography
                    id="modal-modal-title"
                    variant="h6"
                    component="h2"
                  >
                    Log out
                  </Typography>
                </div>
                <div style={{ textAlign: "center" }}>
                  <Typography
                    id="modal-modal-title"
                    variant="h6"
                    component="h2"
                  >
                    Are you sure you want log out{" "}
                  </Typography>
                </div>

                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    <div
                      className="color"
                      style={{
                        backgroundColor: "#fff",
                        borderRadius: 100,
                        padding: "0 30px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <div className="cancelSidebar" onClick={handleClose}>
                        Cancel
                      </div>
                    </div>
                    <div
                      className="color"
                      style={{
                        backgroundColor: "#5EA3D6",
                        borderRadius: 100,
                        padding: "0 30px",
                        display: "flex",
                        justifyContent: "center",
                      }}
                    >
                      <Menu onClick={logout} title="Logout" />
                    </div>
                  </div>
                </Typography>
              </Box>
            </Modal>
          </div>
        ) : null} */}
      </>
    );
  }
}

/**
 *
 *
 */
