import { Box, Container, Divider, Grid } from "@mui/material";
import Layout from "../components/Layout.component";
import { Content } from "../styles";
import { useConnect } from "../utilities/connectHooks";
import Products from "../components/Products.component";
import { RANDOM_PRODUCT } from "../constants/_api.constant";
import Carousel from "../components/Carousel.component";
import "../pages/Home.css";

function ProductCategory() {
  const { data, isFetching, hasError } = useConnect({
    path: `https://staging.drugstoc.com/api/v2/inventory/products?category=&manufacturer`,
  });

  return (
    <div id="home">
      <Layout>
        <Box sx={{ marginTop: 3 }}>
          <Container fixed>
            <Divider sx={{ marginTop: 2 }} />
            {!isFetching ? (
              <Grid sx={{ marginTop: 1 }} container spacing={3}>
                {data?.results
                  .sort((a: any, b: any) => b?.quantity - a?.quantity)
                  .map((element: any, index: number) => (
                    <Grid key={index} item xs={6} md={3}>
                      <Products
                        id={element?.id}
                        title={element?.name}
                        image={element?.image}
                        description={element?.desc}
                        price={element?.price}
                        slug={element?.slug}
                        trigger={RANDOM_PRODUCT}
                        quantity_in_cart={element?.quantity_in_cart}
                        in_cart={element?.in_cart}
                        quantity={element?.quantity}
                        is_favorite={element?.is_favorite}
                      />
                    </Grid>
                  ))}
              </Grid>
            ) : hasError ? null : (
              <Content>Loading.....</Content>
            )}
          </Container>
        </Box>
      </Layout>
    </div>
  );
}

export default ProductCategory;
