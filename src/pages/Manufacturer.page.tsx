import { Box, Card, Grid, Link } from "@mui/material";
import Layout from "../components/Layout.component";
import { PRODUCT_MANUFACTURERS } from "../constants/_api.constant";
import { useConnect } from "../utilities/connectHooks";

function Manufacturer() {
  const {
    data: Products,
    isFetching,
    hasError,
  } = useConnect({ path: PRODUCT_MANUFACTURERS });

  return (
    <div id="manufacturer">
      <Layout>
        <Box mt={15}>
          {!isFetching && hasError === undefined ? (
            <Grid container spacing={3}>
              {Products.results.map((element: any, index: number) => (
                <Grid key={index} item md={3} xs={12}>
                  <Card variant="outlined">
                    <Link href={`/manufacturer/${element.slug}`}>
                      <Box
                        component="img"
                        sx={{
                          width: "100%",
                          height: 200,
                          padding: 2,
                          objectFit: "contain",
                          marginLeft: "auto",
                          marginRight: "auto",
                        }}
                        alt="Manufacturer Images"
                        src={element.logo ? element.logo : "/logo_white.svg"}
                      />
                    </Link>
                  </Card>
                </Grid>
              ))}
            </Grid>
          ) : null}
        </Box>
      </Layout>
    </div>
  );
}

export default Manufacturer;
