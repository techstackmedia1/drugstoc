import { Box, Grid } from "@mui/material";
import { Table } from "evergreen-ui";
import Layout from "../components/Layout.component";
import { CART_ITEMS } from "../constants/_api.constant";
import { orderForm } from "../constants/_form.constant";
import Cards from "../element/card";
import FormRender from "../forms/formRender";
import { Content } from "../styles";
import { useConnect } from "../utilities/connectHooks";
import { priceFormatDecimal } from "../utilities/priceFormatter";
import MultipleSelectPlaceholder from "../components/form.components";

function Checkout() {
  const { data } = useConnect({ path: CART_ITEMS });
  console.log(data)

  return (
    <Layout>
      <Box mt={5}>
        <Grid container spacing={4}>
          <Grid item xs={12} md={8}>
            <Table>
              <Table.Head>
                <Table.TextHeaderCell>Image</Table.TextHeaderCell>
                <Table.TextHeaderCell>Name</Table.TextHeaderCell>
                <Table.TextHeaderCell>Quantity</Table.TextHeaderCell>
                <Table.TextHeaderCell>total amount</Table.TextHeaderCell>
              </Table.Head>
              <Table.Body height="auto">
                {data?.results.map((profile: any, index: number) => (
                  <Table.Row key={index}>
                    <Table.TextCell>
                      <Box
                        component="img"
                        loading="lazy"
                        sx={{
                          width: "100%",
                          height: { xs: 60, md: 60 },
                          borderRadius: 2,
                          cursor: "pointer",
                          objectFit: "contain",
                        }}
                        alt="product image"
                        src={profile.product.image}
                      />
                    </Table.TextCell>
                    <Table.TextCell>{profile.product.name}</Table.TextCell>
                    <Table.TextCell textAlign="center">
                      {profile.quantity}
                    </Table.TextCell>
                    <Table.TextCell isNumber>
                      {priceFormatDecimal(
                        profile.product.price * profile.quantity
                      )}
                    </Table.TextCell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>

            <Box mt={2} />
            <Cards title="Select Options">
              <Box>
                {/* <FormRender data={orderForm} submitButtonText="Create Order" /> */}
                
                <FormRender data={orderForm} submitButtonText="Create Order" />
              </Box>
            </Cards>
          </Grid>

          <Grid item xs={12} md={4}>
            {/* <Cards title='Summary' empty /> */}
            {/* <Box sx={{ marginTop: 5 }}></Box> */}
            <Cards title="Returns are easy">
              <Content sx={{ fontSize: 12, marginTop: 2 }}>
                Free return within 15 days for Official Store items and 7 days
                for other eligible items
              </Content>
            </Cards>
          </Grid>
        </Grid>
      </Box>
    </Layout>
  );
}

export default Checkout;
