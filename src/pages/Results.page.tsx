import { Box, Grid } from '@mui/material'
import { useState } from 'react'
import ProductListLayout from '../components/ProductManufacturer';
import Layout from '../components/Layout.component'
import { getResultUrl, useConnect } from '../utilities/connectHooks';
import { useLocation } from 'react-router-dom';
import Products from '../components/Products.component';
import { Content } from '../styles';

function Results() {
  const router = useLocation();
  const { url } = getResultUrl(router)
  const [params, setParams] = useState('')
  const { data, isFetching, hasError } = useConnect({ path: url + params })

  return (
    <Layout>
      <Box>
        <ProductListLayout setParams={setParams}>
          <Box>
            {!isFetching ? (
              <Grid sx={{ marginTop: 1 }} container spacing={3}>
                {data?.results.map((element: any, index: number) => (
                  <Grid key={index} item xs={12} md={3}>
                    <Products
                      id={element?.id}
                      title={element?.name}
                      image={element?.image}
                      description={element?.desc}
                      trigger={url+params}
                      price={element?.price}
                      slug={element?.slug}
                      // sku={element?.SKU}
                      quantity_in_cart={element?.quantity_in_cart}
                      in_cart={element?.in_cart}
                      quantity={element?.quantity}
                      // category={element?.category}
                      is_favorite={element?.is_favorite}
                    // manufacturer={element?.manufacturer}
                    // composition={element?.composition}
                    />
                  </Grid>
                ))}
              </Grid>
            ) : hasError ? null : <Content>Loading.....</Content>}
          </Box>
        </ProductListLayout>
      </Box>
    </Layout>
  )
}

export default Results