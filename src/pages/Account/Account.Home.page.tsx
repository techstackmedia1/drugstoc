import { Alert, Box, Divider, Link, Stack } from "@mui/material";
import AccountLayout from "../../components/AccountLayout.component";
import { BUSINESS_PROFILE, USER_PROFILE } from "../../constants/_api.constant";
import Cards from "../../element/card";
import { Content, Title } from "../../styles";
import { useConnect } from "../../utilities/connectHooks";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";
import "./AccountHomePage.css";

function AccountHome() {
  const navigate = useNavigate();
  const { data: personal } = useConnect({ path: USER_PROFILE });
  const { data: business } = useConnect({ path: BUSINESS_PROFILE });

  console.log(business);

  const handleClick = () => {
    navigate("/account/shipping_address");
  };

  const checkLicence =
    business?.category === "pharmacy"
      ? "Please Upload your Premise License For The Purpose of Industry Regulations"
      : business?.category === "doctors-office"
      ? "Please Upload your Premise License For The Purpose of Industry Regulations"
      : business?.category === "hospital"
      ? "Please Upload your Premise License For The Purpose of Industry Regulations"
      : business?.category === "nursing-home"
      ? "Please Upload your Premise License For The Purpose of Industry Regulations"
      : business?.category === "private-patient"
      ? "Please Upload your Premise License For The Purpose of Industry Regulations"
      : business?.category === "doctor"
      ? "Please Upload your Practice License For The Purpose of Industry Regulations"
      : business?.category === "nurse"
      ? "Please Upload your Practice License For The Purpose of Industry Regulations"
      : business?.category === "pharmacist"
      ? "Please Upload your Practice License For The Purpose of Industry Regulations"
      : "Please We Are Reviewing Your Premise License";
  const check =
    business?.category === "pharmacy"
      ? "Please Upload your Practice License For The Purpose of Industry Regulations"
      : business?.category === "doctors-office"
      ? "Please Upload your Practice License For The Purpose of Industry Regulations"
      : business?.category === "hospital"
      ? "Please Upload your Practice License For The Purpose of Industry Regulations"
      : business?.category === "nursing-home"
      ? "Please Upload your Practice License For The Purpose of Industry Regulations"
      : business?.category === "private-patient"
      ? "Please Upload your Practice License For The Purpose of Industry Regulations"
      : "Please We Are Reviewing Your Premise License";

  const styleLicense = {
    display: business?.premise_license ? "none" : "block",
  };

  return (
    <div id="personalInfo">
      <AccountLayout>
        <Box>
          <Box sx={{ marginBottom: 5 }}>
            <Title variant="h4" sx={{ paddingTop: 1, paddingBottom: 1 }}>
              Welcome to your account
            </Title>
            <Content>Let's get you started with DrugStoc</Content>
          </Box>
          {!business?.verified_date ? (
            !business?.is_verified ? (
              <Box>
                {/* <Content>Before you place your order</Content> */}
                {business?.premise_license == null ? (
                  <Link href="/account/business">
                    <Alert
                      sx={{ marginBottom: 4, marginTop: 1 }}
                      severity="error"
                    >
                      {checkLicence}
                    </Alert>
                  </Link>
                ) : (
                  <Link href="/account/business">
                    <Alert
                      sx={{ marginBottom: 4, marginTop: 1 }}
                      severity="warning"
                    >
                      Please We Are Reviewing Your Premise License
                    </Alert>
                  </Link>
                )}
                {business?.practice_license === null ? (
                  <Link href="/account/business">
                    <Alert
                      sx={{ marginBottom: 4, marginTop: 1 }}
                      severity="error"
                    >
                      Please Upload your Practice License For the Purpose of
                      Industry Regulations
                    </Alert>
                  </Link>
                ) : (
                  <Link href="/account/business">
                    <Alert
                      sx={{ marginBottom: 4, marginTop: 1 }}
                      severity="warning"
                    >
                      Please We Are Reviewing Your Practice License
                    </Alert>
                  </Link>
                )}
              </Box>
            ) : null
          ) : (
            <>
              <Link href="/account/business">
                <Alert
                  sx={{ marginBottom: 4, marginTop: 1 }}
                  severity="warning"
                >
                  Your Premise License is being verified...
                </Alert>
              </Link>
              <Link href="/account/business">
                <Alert
                  sx={{ marginBottom: 4, marginTop: 1 }}
                  severity="warning"
                >
                  Your Practice License is being verified...
                </Alert>
              </Link>
            </>
          )}

          <Cards
            title="Personal Information"
            subtitle="Addresses, contact information and password"
          >
            <Box>
              <Stack
                direction="row"
                spacing={2}
                divider={<Divider orientation="vertical" flexItem />}
                sx={{ justifyContent: "space-between" }}
              >
                <Box sx={{ width: "50%", marginBottom: 1 }}>
                  <Title>Email Address</Title>
                  {personal?.email ? (
                    <Content sx={{ fontSize: 12 }}>{personal?.email}</Content>
                  ) : (
                    <Content>........</Content>
                  )}
                </Box>
                <Box sx={{ width: "50%", marginBottom: 1 }}>
                  <Title>Phone Number</Title>
                  {personal?.phone ? (
                    <Content sx={{ fontSize: 12 }}>{personal?.phone}</Content>
                  ) : (
                    <Content>........</Content>
                  )}
                </Box>
              </Stack>
              <Divider />
              <Stack
                direction="row"
                spacing={2}
                sx={{ justifyContent: "space-between", marginTop: 2 }}
              >
                <Box>
                  <Title>Add an Address</Title>
                  <Content sx={{ fontSize: 12 }}>
                    Adding an address allows for quicker checkout and
                    approximation of delivery times
                  </Content>
                </Box>
                <Button
                  sx={{
                    width: 150,
                    height: 70,
                    fontSize: 14,
                    padding: "0 100px",
                    borderRadius: 100,
                  }}
                  variant="outlined"
                  disableElevation
                  onClick={handleClick}
                >
                  Address
                </Button>
              </Stack>
            </Box>
          </Cards>
          <Box sx={{ marginTop: 4 }}>
            <div className="purchase-history">
              <Cards title="Purchase History" empty></Cards>
            </div>
          </Box>
          <Box sx={{ marginTop: 4 }}>
            <Cards title="DrugStoc Credit" empty></Cards>
          </Box>
        </Box>
      </AccountLayout>
    </div>
  );
}

export default AccountHome;
