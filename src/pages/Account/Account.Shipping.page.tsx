import { Add } from "@mui/icons-material";
import { Box, Button } from "@mui/material";
import {
  CogIcon,
  Dialog,
  EmptyState,
  IconButton,
  majorScale,
  Menu,
  Popover,
  Position,
  SearchIcon,
  Table,
} from "evergreen-ui";
import { useState } from "react";
import AccountLayout from "../../components/AccountLayout.component";
import { SHIPPING_ADDRESS } from "../../constants/_api.constant";
import { shippingAddress } from "../../constants/_form.constant";
import Cards from "../../element/card";
import FormRender from "../../forms/formRender";
import { Content, Title } from "../../styles";
import { request, useConnect } from "../../utilities/connectHooks";
import "./Account.Shipping.page.css";

function Shipping() {
  const [open, setOpen] = useState(false);

  const { data } = useConnect({ path: SHIPPING_ADDRESS });

  const submit = (e: any) => {
    e.is_default_address = true;
    request
      .post({ path: SHIPPING_ADDRESS, trigger: SHIPPING_ADDRESS, payload: e })
      .then((res) => {
        if (res) {
          setOpen(false);
        }
      });
  };

  const deleteAddress = (id: any) => {
    request.delete({
      path: `${SHIPPING_ADDRESS}/${id}`,
      trigger: SHIPPING_ADDRESS,
    });
  };

  const Modal = () => {
    return (
      <Box>
        <Dialog
          isShown={open}
          title="Add Shipping Address"
          preventBodyScrolling
          hasFooter={false}
          onCloseComplete={() => setOpen(!open)}
        >
          <FormRender data={shippingAddress} onSubmitForm={submit} />
        </Dialog>

        <Button
          disableElevation
          variant="contained"
          onClick={() => setOpen(!open)}
          sx={{ height: 40 }}
          startIcon={<Add />}
        >
          Add New Address
        </Button>
      </Box>
    );
  };

  return (
    <div id="shipping">
      <AccountLayout>
        <Box>
          <Box mb={5}>
            <Title variant="h4" sx={{ paddingTop: 1, paddingBottom: 1 }}>
              Shipping Address
            </Title>
            <Content>Let's get you started with DrugStoc</Content>
          </Box>
          <Cards title="Your shipping Addresses" action={<Modal />}>
            <Box>
              <Table>
                {/* <Table.Head>
                  <Table.TextHeaderCell>Name</Table.TextHeaderCell>
                  <Table.TextHeaderCell>Address</Table.TextHeaderCell>
                  <Table.TextHeaderCell>Phone number</Table.TextHeaderCell>
                  <Table.TextHeaderCell>City</Table.TextHeaderCell>
                  <Table.TextHeaderCell>action</Table.TextHeaderCell>
                </Table.Head> */}
                <Table.Body height="auto">
                  {data?.results.length === 0 ? (
                    <EmptyState
                      background="light"
                      title="No tracked events for this Source"
                      orientation="horizontal"
                      icon={<SearchIcon color="#C1C4D6" />}
                      iconBgColor="#EDEFF5"
                      description="Events appear when the Source starts sending data to Segment about your users and their activity."
                    />
                  ) : (
                    data?.results.map((profile: any) => (
                      <Table.Row key={profile.id} isSelectable>
                        <Table.TextCell>
                          {profile.first_name} {profile.last_name}
                        </Table.TextCell>
                        <Table.TextCell>
                          {profile.address_line1} {profile.address_line2}
                        </Table.TextCell>
                        <Table.TextCell textAlign="center" isNumber>
                          {profile.phone_number1}
                        </Table.TextCell>
                        <Table.TextCell>{profile.region}</Table.TextCell>
                        <Table.TextCell>
                          <Popover
                            position={Position.BOTTOM_RIGHT}
                            content={
                              <Menu>
                                <Menu.Group>
                                  <Menu.Item
                                    onSelect={() => console.log("oooop")}
                                  >
                                    Edit
                                  </Menu.Item>
                                  <Menu.Item
                                    onSelect={() => deleteAddress(profile.id)}
                                  >
                                    Delete
                                  </Menu.Item>
                                  <Menu.Item
                                    onSelect={() => console.log("oooow")}
                                  >
                                    Set As Default
                                  </Menu.Item>
                                </Menu.Group>
                              </Menu>
                            }
                          >
                            <IconButton
                              icon={CogIcon}
                              marginRight={majorScale(2)}
                            />
                          </Popover>
                        </Table.TextCell>
                      </Table.Row>
                    ))
                  )}
                </Table.Body>
              </Table>
            </Box>
          </Cards>
        </Box>
      </AccountLayout>
    </div>
  );
}

export default Shipping;
