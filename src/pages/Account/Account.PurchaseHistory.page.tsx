import Box from "@mui/material/Box";
import {
  Badge,
  CogIcon,
  EmptyState,
  IconButton,
  majorScale,
  Menu,
  Popover,
  Position,
  SearchIcon,
  Table,
} from "evergreen-ui";
import moment from "moment";
import React from "react";
import AccountLayout from "../../components/AccountLayout.component";
import { PURCHASE_HISTORY } from "../../constants/_api.constant";
import { Content, Title } from "../../styles";
import { useConnect } from "../../utilities/connectHooks";
import { priceFormatDecimal } from "../../utilities/priceFormatter";
import "./purchaseHistory.css";
import "./purchaseHistory.css";
import './Account.PurchaseHistory.page'

function PurchaseHistory() {
  const { data } = useConnect({ path: PURCHASE_HISTORY });

  return (
    <div id='purchaseHistory'>
      <AccountLayout>
        <Box>
          <Title variant="h4" sx={{ paddingTop: 1, paddingBottom: 1 }}>
            Purchase History
          </Title>
          <Content>Let’s get you started with DrugStoc</Content>
          <Box mt={5}>
            <Table>
              <Table.Head>
                <Table.TextHeaderCell>order id</Table.TextHeaderCell>
                <Table.TextHeaderCell>status</Table.TextHeaderCell>
                <Table.TextHeaderCell>NUMBER OF ITEMS</Table.TextHeaderCell>
                <Table.TextHeaderCell>total amount</Table.TextHeaderCell>
                <Table.TextHeaderCell>Date</Table.TextHeaderCell>
                <Table.TextHeaderCell>action</Table.TextHeaderCell>
              </Table.Head>
              <Table.Body height="auto">
                {data?.results.length === 0 ? (
                  <EmptyState
                    background="light"
                    title="No tracked events for this Source"
                    orientation="horizontal"
                    icon={<SearchIcon color="#C1C4D6" />}
                    iconBgColor="#EDEFF5"
                    description="Events appear when the Source starts sending data to Segment about your users and their activity."
                  />
                ) : (
                  data?.results.map((profile: any) => (
                    <Table.Row key={profile.id} isSelectable>
                      <Table.TextCell>{profile.order_no}</Table.TextCell>
                      <Table.TextCell>
                        <Badge
                          color={
                            profile.status === "pending" ? "blue" : "green"
                          }
                        >
                          {profile.status}
                        </Badge>
                      </Table.TextCell>
                      <Table.TextCell textAlign="center">
                        {profile.items.length} Items
                      </Table.TextCell>
                      <Table.TextCell isNumber>
                        {priceFormatDecimal(profile.total_amount)}
                      </Table.TextCell>
                      <Table.TextCell>
                        {moment(profile.created_at).fromNow()}
                      </Table.TextCell>
                      <Table.TextCell isNumber>
                        <Popover
                          position={Position.BOTTOM_RIGHT}
                          content={
                            <Menu>
                              <Menu.Group>
                                <Menu.Item
                                  onSelect={() => console.log("oooop")}
                                >
                                  Re-Order
                                </Menu.Item>
                                <Menu.Item
                                  onSelect={() => console.log("oooow")}
                                >
                                  View Details
                                </Menu.Item>
                              </Menu.Group>
                            </Menu>
                          }
                        >
                          <IconButton
                            icon={CogIcon}
                            marginRight={majorScale(2)}
                          />
                        </Popover>
                      </Table.TextCell>
                    </Table.Row>
                  ))
                )}
              </Table.Body>
            </Table>

            <div className="wallet">
              <div className="position">
                <div>
                  <p>ORDER ID</p>
                </div>
                <div></div>
                <div>
                  <p>STATUS</p>
                </div>
                <div></div>
                <div>
                  <p>NUMBER OF ITEMS</p>
                </div>
                <div></div>
                <div>
                  <p>TOTAL AMOUNT</p>
                </div>
                <div></div>
                <div>
                  <p>DATE</p>
                </div>
                <div></div>
                <div>
                  <p>ACTION</p>
                </div>
                <div></div>
              </div>
            </div>
          </Box>
        </Box>
      </AccountLayout>
    </div>
  );
}

export default PurchaseHistory;
