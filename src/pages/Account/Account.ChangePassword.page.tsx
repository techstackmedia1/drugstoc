import AccountLayout from "../../components/AccountLayout.component";
import changePassword from "./Account.ChangePassword.page.module.css";

function ChangePassword() {
  return (
    <AccountLayout>
      <div className={changePassword.changePassword}>
        <h2>Change Password</h2>
        <p className={changePassword.text}>
          Let's get you started with DrugStoc
        </p>
        <form action="" method="post">
          <div className={changePassword.reset}>
            <h3>Password Reset</h3>
          </div>
          <div className={changePassword.passwordConfirm}>
            <div>
              <h4>New Password</h4>
              <input type="password" name="" id="" />
            </div>
            <div>
              <h4>Confirm Password</h4>
              <input type="password" name="" id="" />
            </div>
          </div>
          <div>
            <button type="submit">Done</button>
          </div>
        </form>
      </div>
    </AccountLayout>
  );
}

export default ChangePassword;
