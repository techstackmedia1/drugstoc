import { Box, Grid } from "@mui/material";
import React from "react";
import AccountLayout from "../../components/AccountLayout.component";
import Products from "../../components/Products.component";
import { FAVORITE_PRODUCT } from "../../constants/_api.constant";
import { Content, Title } from "../../styles";
import { useConnect } from "../../utilities/connectHooks";

function Favorite() {
  const { data, isFetching, hasError } = useConnect({ path: FAVORITE_PRODUCT });
  return (
    <AccountLayout>
      <Box>
        <Title variant="h4" sx={{ paddingTop: 1, paddingBottom: 1 }}>
          My Favorite Products
        </Title>
        <Content>Let’s get you started with DrugStoc</Content>
        {!isFetching ? (
          <Grid sx={{ marginTop: 1 }} container spacing={3}>
            {data?.results.map((element: any, index: number) => (
              <Grid key={index} item xs={12} md={4}>
                <Products
                  id={element?.product.id}
                  title={element?.product.name}
                  image={element?.product.image}
                  description={element?.product.desc}
                  price={element?.product.price}
                  slug={element?.product.slug}
                  trigger={FAVORITE_PRODUCT}
                  in_fav={true}
                  // sku={element?.SKU}
                  quantity_in_cart={element?.product.quantity_in_cart}
                  in_cart={element?.product.in_cart}
                  quantity={element?.product.quantity}
                  // category={element?.category}
                  is_favorite={element?.product.is_favorite}
                  // manufacturer={element?.manufacturer}
                  // composition={element?.composition}
                />
              </Grid>
            ))}
          </Grid>
        ) : hasError ? null : (
          <Content>Loading.....</Content>
        )}
      </Box>
    </AccountLayout>
  );
}

export default Favorite;
