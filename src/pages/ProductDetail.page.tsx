import { Add, Remove } from '@mui/icons-material'
import { Box, Divider, Grid, Stack } from '@mui/material'
import React, { useState } from 'react'
import { useLocation } from 'react-router-dom'
import Layout from '../components/Layout.component'
import { AddToCartButton, Content, IconButton, Title } from '../styles'
import { getResultUrl, useConnect } from '../utilities/connectHooks'
import { priceFormat } from '../utilities/priceFormatter'

function ProductDetail() {
    const router = useLocation();
    const { url } = getResultUrl(router)
    const { data, isFetching, hasError } = useConnect({ path: url });

    const [quantity, setQuantity] = useState(1);
    const [loading_decrease, setLoadingDecrease] = useState(false);
    const [loading_increase, setLoadingIncrease] = useState(false);
    const [inFav, setFav] = useState(false);
    const [inCart, setCart] = useState(false);
    const [loading_cart, setLoadingCart] = useState(false);
    const [loading_fav, setLoadingFav] = useState(false);
    const [loading_del, setLoadingDelete] = useState(false);

    return (
        <Layout>
            <Box mt={{xs: 5, md: 10}}>
            {!isFetching && hasError === undefined ? (
            <Box>
              <Grid container spacing={4}>
                <Grid item md={8} xs={12}>
                  <Box
                    component="img"
                    loading="lazy"
                    sx={{
                      width: "100%",
                      height: { xs: 300, md: 430 },
                      border: "1px solid #d7d7d7", 
                      borderRadius: 2,
                      cursor: "pointer",
                      objectFit: "contain",
                    }}
                    alt="product image"
                    src={data.image}
                  />
                </Grid>
                <Grid item md={4} xs={12}>
                  <Title
                    variant="h5"
                    sx={{
                      lineHeight: 1.4,
                    }}
                  >
                    {data.name}
                  </Title>
                  <Content
                    sx={{
                      fontSize: 12,
                      textOverflow: "ellipsis",
                      overflow: "hidden",
                      whiteSpace: "nowrap",
                    }}
                  >
                    <Title>SKU: {data.SKU}</Title>
                  </Content>
                  <Content
                    sx={{
                      fontSize: 12,
                      textOverflow: "ellipsis",
                      overflow: "hidden",
                      whiteSpace: "nowrap",
                    }}
                  >
                    <Title>MANUFACTURER: {data.manufacturer}</Title>
                  </Content>
                  <Content
                    sx={{
                      fontSize: 12,
                      textOverflow: "ellipsis",
                      overflow: "hidden",
                      whiteSpace: "nowrap",
                    }}
                  >
                    DESCRIPTION: {data.desc}
                  </Content>
                  <Divider sx={{ marginTop: 2.5, marginBottom: 2.5 }} />
                  <Title
                    variant="h5"
                    sx={{
                      filter: data.price ? "blur(0px)" : "blur(5px)",
                    }}
                  >
                    {data.price
                      ? priceFormat(data.price)
                      : "PRICE NOT AVAILABLE"}
                  </Title>
                  <Divider sx={{ marginTop: 2.5, marginBottom: 2.5 }} />
                  {!inCart ? (
                    <AddToCartButton
                      variant="contained"
                      disabled={data.quantity === 0}
                    //   onClick={add_to_cart}
                    //   loading={loading_cart}
                      sx={{ width: "100%", fontSize: 12, fontWeight: "900" }}
                      disableElevation
                    >
                      {data.quantity === 0 ? "Out of Stock" : "Add to cart"}
                    </AddToCartButton>
                  ) : (
                    <Stack
                      direction="row"
                      justifyContent="space-between"
                      width="100%"
                      spacing={3}
                    >
                      <IconButton
                        // onClick={decrease_item}
                        // loading={loading_decrease}
                        variant="contained"
                      >
                        <Remove />
                      </IconButton>
                      <Title>{quantity}</Title>
                      <IconButton
                        // onClick={increase_item}
                        // loading={loading_increase}
                        variant="contained"
                      >
                        <Add />
                      </IconButton>
                    </Stack>
                  )}
                </Grid>
              </Grid>
              <Divider sx={{ marginTop: 2.5, marginBottom: 2.5 }} />
              <Title
                variant="h6"
                sx={{
                  lineHeight: 1.4,
                }}
              >
                ALTERNATIVE PRODUCTS
              </Title>
            </Box>
          ) : null}
            </Box>
        </Layout>
    )
}

export default ProductDetail