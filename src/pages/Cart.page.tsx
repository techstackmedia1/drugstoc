import { Box, Button, Divider, Grid, Stack } from "@mui/material";
import CartItem from "../components/CartItem.component";
import Layout from "../components/Layout.component";
import { CART_ITEMS } from "../constants/_api.constant";
import Cards from "../element/card";
import { Content } from "../styles";
import { useConnect } from "../utilities/connectHooks";
import "./Cart.css";

function Cart() {
  const { data } = useConnect({ path: CART_ITEMS });
  if (data?.results.length === 0) {
    <Cards title="Cart is Empty" empty={data?.results.length === 0}></Cards>;
  } else {
    <Cards title="My Cart" empty={data?.results.length === 0}>
      {data?.results.map((element: any, i: number) => (
        <CartItem key={i} data={element} />
      ))}
    </Cards>;
  }

  const redirectUrl =
    data?.results.length === 0 ? (
      <div style={{ cursor: "not-allowed", pointerEvents: "auto" }}>
        <Button variant="outlined" disabled>
          Checkout
        </Button>
      </div>
    ) : (
      <Button variant="contained" href="/checkout">
        Checkout
      </Button>
    );

  const isCartEmpty =
    data?.results.length === 0 ? (
      <Cards title="Cart is Empty"></Cards>
    ) : (
      <Cards title="My Cart" empty={data?.results.length === 0}>
        {data?.results.map((element: any, i: number) => (
          <CartItem key={i} data={element} />
        ))}
      </Cards>
    );
  return (
    <div id="cart">
      <Layout>
        <Box mt={5}>
          <Grid container spacing={4}>
            <Grid item xs={12} md={8}>
              {isCartEmpty}
            </Grid>
            <Grid item xs={12} md={4}>
              <Cards title="Summary">
                <Box>
                  <Stack
                    direction="row"
                    sx={{ marginTop: 2 }}
                    justifyContent="space-between"
                    alignItems="center"
                  >
                    <Box>
                      <Content>Subtotal</Content>
                    </Box>
                    {/* <Title variant="h6">{priceFormat(getTotal())}.00</Title> */}
                  </Stack>
                  <Content sx={{ fontSize: 12 }}>
                    Delivery fees not included yet.
                  </Content>
                  <Divider sx={{ marginTop: 2, marginBottom: 2 }} />
                  {redirectUrl}
                </Box>
              </Cards>
              <Box sx={{ marginTop: 5 }}></Box>
              <Cards title="Returns are easy">
                <Content sx={{ fontSize: 12, marginTop: 2 }}>
                  Free return within 15 days for Official Store items and 7 days
                  for other eligible items
                </Content>
              </Cards>
            </Grid>
          </Grid>
        </Box>
      </Layout>
    </div>
  );
}

export default Cart;
