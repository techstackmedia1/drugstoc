import {
  Box,
  Container,
  Button,
  Divider,
  Grid,
  Paper,
  Stack,
} from "@mui/material";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import Layout from "../components/Layout.component";
import { Content, Title } from "../styles";
import Banners from "../element/banners";
import { useConnect } from "../utilities/connectHooks";
import Products from "../components/Products.component";
import { RANDOM_PRODUCT } from "../constants/_api.constant";
import Carousel from "../components/Carousel.component";
import "./homemui.css";
import GetManufacturerProducts from "../fetchAPI/GetManufacturersProducts";
import GetProductCategory from "../fetchAPI/GetProductCategory";
import "./Home.page.css";

function Home() {
  const { data, isFetching, hasError } = useConnect({ path: RANDOM_PRODUCT });

  return (
    <div id="home">
      <Layout banners={<Carousel />}>
        <Box sx={{ marginTop: 3 }}>
          <Container fixed>
            <Paper elevation={0}>
              <Title variant="h5">Don't Miss Out On These.</Title>
              <Divider sx={{ marginTop: 2 }} />
              <Banners />
            </Paper>
            <Stack
              direction="row"
              sx={{ marginTop: 4 }}
              justifyContent="space-between"
            >
              <Box>
                <Title variant="h4">Popular Products</Title>
                <Content sx={{ fontSize: 12 }}>
                  Browse through popular products.
                </Content>
              </Box>
              {/* <Button
                variant="text"
                onClick={() => null}
                sx={{ textTransform: "capitalize" }}
                endIcon={<ArrowForwardIcon />}
              >
                See all popular products
              </Button> */}
            </Stack>
            <Divider sx={{ marginTop: 2 }} />
            {!isFetching ? (
              <Grid sx={{ marginTop: 1 }} container spacing={3}>
                {data?.results
                  .sort((a: any, b: any) => b?.quantity - a?.quantity)
                  .map((element: any, index: number) => (
                    <Grid key={index} item xs={6} md={3}>
                      <Products
                        id={element?.id}
                        title={element?.name}
                        image={element?.image}
                        // const text = note.split(' ')
                        // text.splice(10, note.length - 10, '...')
                        //const para = text.join(' ').replace(" ...", "...").replace("....", "...");
                        description={element?.desc}
                        price={element?.price}
                        slug={element?.slug}
                        trigger={RANDOM_PRODUCT}
                        // sku={element?.SKU}
                        quantity_in_cart={element?.quantity_in_cart}
                        in_cart={element?.in_cart}
                        quantity={element?.quantity}
                        // category={element?.category}
                        is_favorite={element?.is_favorite}
                        // manufacturer={element?.manufacturer}
                        // composition={element?.composition}
                      />
                    </Grid>
                  ))}
              </Grid>
            ) : hasError ? null : (
              <Content>Loading.....</Content>
            )}
          </Container>
        </Box>
      </Layout>
    </div>
  );
}

export default Home;
