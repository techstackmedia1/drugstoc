import { Box, Grid, Link } from '@mui/material'
import React from 'react'

function Banners() {
    const itemData = [
        {
            img: "/banner4.jpeg",
            title: "Breakfast",
            link: "/products/wellman-prostate-x60",
            cols: 2,
        },
        {
            img: "/banner2.jpeg",
            title: "Coffee",
            cols: 2,
            link: "/products/aiita-kn95-facemask-x-5",
        },
    ];

    return <Grid sx={{ marginTop: 1 }} container spacing={3}>
        {itemData.map((item: any, index) => (
            <Grid key={index} item xs={12} md={6}>
                <Link style={{ cursor: "pointer" }} href={item?.link}>
                    <Box
                        component="img"
                        sx={{
                            width: "100%",
                            height: 230,
                            objectFit: { xs: "contain", md: "cover" },
                        }}
                        alt="ads image"
                        src={item?.img}
                    />
                </Link>
            </Grid>
        ))}
    </Grid>
}

export default Banners