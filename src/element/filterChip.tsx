import { KeyboardArrowDown } from "@mui/icons-material";
import { Box, Chip, Menu } from "@mui/material";
import { useEffect, useState } from "react";
import { filterModel } from "../models/_filter.models";
import { optionsModel } from "../models/_options.models";
import { TextField } from "../styles";
import FilterOption from "./filterOption";
import { Category } from "./icons";

function FilterChip({
  name = "test",
  icon = <Category />,
  searchable = false,
  apply,
  ...props
}: filterModel) {
  const [anchorEl, setAnchorEl] = useState(null);
  const [items, setItems] = useState<Array<optionsModel>>([]);
  const [select, setSeletion] = useState<boolean>(false);
  const [range, setRange] = useState([0, 100]);
  const [radio, setRadio] = useState("");
  const [selectValue, setSeletionValue] = useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {}, [selectValue]);

  const onChange = (data: any) => {
      apply(data)
  } 
  


  return (
    <Box>
      <Chip
        icon={icon}
        sx={{ pl: 1 }}
        color={select ? "primary" : "default"}
        onClick={handleClick}
        onDelete={() => null}
        deleteIcon={<KeyboardArrowDown />}
        label={`${name}`}
      />
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: "48%",
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "center", vertical: "top" }}
        anchorOrigin={{ horizontal: "center", vertical: "bottom" }}
      >
        {searchable ? (
          <div>
            <Box sx={{ padding: 2, width: 400 }}>
              <TextField
                sx={{ borderRadius: 30 }}
                placeholder={`Find a ${"brand"}`}
                // onChange={(e) => search(e.target.value)}
                // placeholder={`Search ${props.label}`}
              />
            </Box>
          </div>
        ) : null}
        <FilterOption
          {...props}
          setItems={setItems}
          items={items}
          setSeletion={setSeletion}
          setRadio={setRadio}
          radio={radio}
          setRange={setRange}
          rangeValue={range}
          onChange={onChange}
          setSeletionValue={setSeletionValue}
        />
      </Menu>
    </Box>
  );
}

export default FilterChip;
