import React from "react";
import { Content, Title } from "../styles";
import {
  Business,
  Favorite,
  Help,
  History,
  Personal,
  Shipping,
  Terms,
  Wallet,
} from "./icons";
import Menu from "./menu";

import { Button, Typography, Modal,Box, Link } from "@mui/material";

import "./sidebar.css";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function Sidebar({ user }: any) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const logout = () => {
    localStorage.removeItem("token");
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Box sx={{ borderBottom: "1px solid #d7d7d7" }}>
        <Link
          href="/account"
          underline="none"
          sx={{ fontWeight: 600, color: "#000" }}
          variant="h4"
        >
          Hi, {user?.first_name ? user.first_name : "......"}
        </Link>
        <Content sx={{ marginBottom: "10px !important", fontSize: 10 }}>
          Thanks for being a DrugStoc customer
        </Content>
      </Box>
      <Box
        sx={{
          paddingTop: 1,
          paddingBottom: 1,
          borderBottom: "1px solid #d7d7d7",
        }}
      >
        <Menu
          Icons={History}
          title="Purchase History"
          navigation="/account/purchase_history"
        />
        <Menu
          Icons={Wallet}
          title="DrugStoc Pay"
          navigation="/account/wallet"
        />
        {/* <Menu Icons={Document} title="Telemedicine (Bulk Order)" navigation="/account/bulk-order" /> */}
      </Box>
      <Box
        sx={{
          paddingTop: 1,
          paddingBottom: 1,
          borderBottom: "1px solid #d7d7d7",
        }}
      >
        <Title variant="h6" sx={{ paddingTop: 1, paddingBottom: 1 }}>
          Manage Account
        </Title>
        <Menu
          Icons={Personal}
          title="Personal information"
          navigation="/account/personal_info"
        />
        <Menu
          Icons={Business}
          title="Business information"
          navigation="/account/business"
        />
        <Menu
          Icons={Shipping}
          title="Shipping Addresses"
          navigation="/account/shipping_address"
        />
      </Box>
      <Box
        sx={{
          paddingTop: 1,
          paddingBottom: 1,
          borderBottom: "1px solid #d7d7d7",
        }}
      >
        <Title variant="h6" sx={{ paddingTop: 1, paddingBottom: 1 }}>
          My DrugStoc
        </Title>
        {/* <Menu Icons={ReOrder} title="Reorder" navigation="/account/reorder" /> */}
        <Menu
          Icons={Favorite}
          title="My Favorite"
          navigation="/account/favorite"
        />
        {/* <Menu Icons={OrderHistory} title="My Orders" navigation="/account/my_order" /> */}
      </Box>
      <Box
        sx={{
          paddingTop: 1,
          paddingBottom: 1,
          borderBottom: "1px solid #d7d7d7",
        }}
      >
        <Title variant="h6" sx={{ paddingTop: 1, paddingBottom: 1 }}>
          Customer Service
        </Title>
        <Menu
          Icons={Help}
          title="Change Password"
          navigation="/account/change-password"
        />
        <Menu Icons={Help} title="Help" />
        <Menu Icons={Terms} title="Term of use" />
      </Box>
      <Box
        sx={{
          paddingTop: 1,
          paddingBottom: 1,
        }}
      >
        <div style={{ width: 350, border: "none" }}>
          <Button onClick={handleOpen}>Sign Out</Button>
          <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box sx={style}>
              <div style={{ textAlign: "center" }}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                  Log out
                </Typography>
              </div>
              <div style={{ textAlign: "center" }}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                  Are you sure you want log out{" "}
                </Typography>
              </div>

              <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <div
                    className="color"
                    style={{
                      backgroundColor: "#fff",
                      borderRadius: 100,
                      padding: "0 30px",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <div className="cancelSidebar" onClick={handleClose}>
                      Cancel
                    </div>
                  </div>
                  <div
                    className="color"
                    style={{
                      backgroundColor: "#5EA3D6",
                      borderRadius: 100,
                      padding: "0 30px",
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    <Menu onClick={logout} title="Logout" />
                  </div>
                </div>
              </Typography>
            </Box>
          </Modal>
        </div>
      </Box>
    </Box>
  );
}

export default Sidebar;
