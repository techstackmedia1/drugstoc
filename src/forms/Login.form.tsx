import { Form, Formik } from "formik";
import { Button } from "../styles";
import { LoginFormData } from "../constants/_form.constant";
import Input from "./_input";
import { Validation } from "./_validations";

function LoginForm() {
  return (
    <Formik
      initialValues={{ email: "", password: "" }}
      validationSchema={Validation.loginValidation}
      onSubmit={(value) => alert(JSON.stringify(value))}
    >
      <Form>
        {LoginFormData.map((element, index) => (
          <Input
            key={index}
            label={element.label}
            name={element.name}
            placeholder={element.placeholder}
            type={element.type}
            form_type={element.form_type}
            options={element.options}
          />
        ))}
        <Button type="submit" variant="contained">
          Login to account
        </Button>
      </Form>
    </Formik>
  );
}

export default LoginForm;
