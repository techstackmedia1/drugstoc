import { useField } from "formik";
import { Forms } from "../models/_form.models";
import { Box } from "@mui/material";
import { optionsModel } from "../models/_options.models";
import { SelectField, TextareaField, TextInputField } from "evergreen-ui";

function Input({ type = "Text", form_type = "text", disabled = false, ...props }: Forms) {
    const [field, meta] = useField(props);

    return (
        <Box width="100%">
            {type === "Select" ? (
                <SelectField
                    width="100%"
                    isInvalid={meta.touched && meta.error !== undefined}
                    validationMessage={meta.error}
                    inputHeight={44}
                    label={props.label}
                    {...field}
                >
                    <option value="" defaultValue="">
                        {props.placeholder}
                    </option>
                    {props?.options?.map((element: optionsModel, index: number) => (
                        <option key={index} value={element.value}>
                            {element.text}
                        </option>
                    ))}
                </SelectField>
            ) : type === "TextArea" ? (
                <TextareaField
                    isInvalid={meta.touched && meta.error !== undefined}
                    validationMessage={meta.error}
                    disabled={disabled}
                    inputHeight={70}
                    {...field}
                    width="100%"
                    label={props.label}
                    placeholder={props.placeholder}
                />
            ) : (
                <TextInputField
                    isInvalid={meta.touched && meta.error !== undefined}
                    validationMessage={meta.error}
                    disabled={disabled}
                    type={form_type}
                    inputHeight={40}
                    {...field}
                    width="100%"
                    label={props.label}
                    placeholder={props.placeholder}
                />
            )}
        </Box>
    );
}

export default Input;
