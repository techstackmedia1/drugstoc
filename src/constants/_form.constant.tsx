import { Forms } from "../models/_form.models";
import GetUserShippingAddress from "../fetchAPI/GetUserShippingAddress";
import GetUserShippingPaymentMethod from "../fetchAPI/GetUserShippingPaymentMethod";

import {
  business_category,
  business_location,
  discovery,
} from "./_options.constant";

export const LoginFormData: Forms[] = [
  {
    label: "Email*",
    name: "email",
    placeholder: "Enter email Address",
  },
  {
    label: "Password*",
    name: "password",
    placeholder: "Enter password",
    form_type: "password",
  },
];

export const RegisterForm1: Forms[] = [
  {
    name: "",
    row: {
      column: 2,
      items: [
        {
          label: "First Name*",
          name: "first_name",
          placeholder: "Enter Your First Name",
        },
        {
          label: "Last Name*",
          name: "last_name",
          placeholder: "Enter Your Last Name",
        },
      ],
    },
  },
  {
    label: "Email*",
    name: "email",
    placeholder: "Enter email Address",
  },
  {
    label: "Phone number",
    name: "phone",
    placeholder: "Enter Phone Number ",
  },
  {
    label: "Password",
    name: "password",
    placeholder: "Enter Password",
    form_type: "password",
  },
];

export const RegisterForm2: Forms[] = [
  {
    label: `Name*`,
    name: "business_name",
    placeholder: `Enter Your name`,
    bool: true,
  },
  {
    label: `Category*`,
    name: "category",
    placeholder: `Select Your Category`,
    type: "Select",
    options: business_category,
    bool: true,
  },
  {
    label: `Location*`,
    name: "location",
    placeholder: `Select Your Location`,
    type: "Select",
    options: business_location,
    bool: true,
  },
  {
    label: "How did you hear about us*",
    name: "discovery",
    placeholder: "Select how you heard about us",
    type: "Select",
    options: discovery,
  },
  {
    label: "Referral Code (Optional)",
    name: "ref_code",
    placeholder: "Enter referral code of who referred you",
  },
];

export const RegisterForm3: Forms[] = [
  {
    label: `Business Name*`,
    name: "business_name",
    placeholder: `Enter Your Business name`,
    bool: false,
  },
  {
    label: `Business category*`,
    name: "category",
    placeholder: `Select Your Business Category`,
    type: "Select",
    options: business_category,
    bool: false,
  },
  {
    label: `Business Location*`,
    name: "location",
    placeholder: `Select Your Business Location`,
    type: "Select",
    options: business_location,
    bool: false,
  },
  {
    label: "How did you hear about us*",
    name: "discovery",
    placeholder: "Select how you heard about us",
    type: "Select",
    options: discovery,
  },
  {
    label: "Referral Code (Optional)",
    name: "ref_code",
    placeholder: "Enter referral code of who referred you",
  },
];

export const orderForm: Forms[] = [
  {
    name: "",
    row: {
      column: 2,
      items: [
        {
          label: "Select your shipping Address*",
          name: "shipping",
          placeholder: <GetUserShippingAddress />,
          type: "Select",
          options: [],
        },
        {
          label: "Select Your Payment method*",
          name: "payment_method",
          placeholder: <GetUserShippingPaymentMethod />,
          type: "Select",
          options: [],
        },
      ],
    },
  },
];

export const UpdateBusinessDate: Forms[] = [
  {
    name: "",
    row: {
      column: 3,
      items: [
        {
          label: "Business Name*",
          name: "name",
          placeholder: "Enter Your Business name",
        },
        {
          label: "Support Email*",
          name: "email",
          placeholder: "Enter Your Business email",
        },
        {
          label: "Support Phone Number*",
          name: "phone",
          placeholder: "Enter Your Business Phone Number",
        },
      ],
    },
  },
  {
    label: "About Business*",
    name: "about",
    placeholder: "Write about your business",
    type: "TextArea",
  },
  {
    label: "Address*",
    name: "address",
    placeholder: "Select Your Business Address",
  },
  {
    name: "",
    row: {
      column: 2,
      items: [
        {
          label: "Business category*",
          name: "category",
          placeholder: "Select Your Business Category",
          type: "Select",
          options: business_category,
        },
        {
          label: "Business Location*",
          name: "location",
          placeholder: "Select Your Business Location",
          type: "Select",
          options: business_location,
        },
      ],
    },
  },
];

export const shippingAddress: Forms[] = [
  {
    name: "",
    row: {
      column: 2,
      items: [
        {
          label: "First Name*",
          name: "first_name",
          placeholder: "Enter Your First Name",
        },
        {
          label: "Last Name*",
          name: "last_name",
          placeholder: "Enter Your Last Name",
        },
      ],
    },
  },
  {
    name: "",
    row: {
      column: 2,
      items: [
        {
          label: "Address*",
          name: "address_line1",
          placeholder: "Enter address",
        },
        {
          label: "City*",
          name: "address_line2",
          placeholder: "Enter City",
        },
      ],
    },
  },
  {
    name: "",
    row: {
      column: 2,
      items: [
        {
          label: "Mobile Phone*",
          name: "phone_number1",
          placeholder: "Enter Your Mobile Phone Number",
        },
        {
          label: "Telephone Number*",
          name: "phone_number2",
          placeholder: "Enter Your Telephone Number",
        },
        {
          label: "Location*",
          name: "region",
          placeholder: "Select Location",
          type: "Select",
          options: business_location,
        },
      ],
    },
  },
  {
    label: "Additional Information*",
    name: "additional_information",
    placeholder: "Write about anything you would like to let us know",
    type: "TextArea",
  },
];

export const PersonalDataForm: Forms[] = [
  {
    name: "",
    row: {
      column: 2,
      items: [
        {
          label: "First Name*",
          name: "first_name",
          placeholder: "Enter Your First Name",
        },
        {
          label: "Last Name*",
          name: "last_name",
          placeholder: "Enter Your Last Name",
        },
      ],
    },
  },
  {
    label: "Email*",
    name: "email",
    placeholder: "Enter email Address",
  },
  {
    label: "Phone number",
    name: "phone",
    placeholder: "Enter Phone Number ",
  },
  {
    label: "Referral Code",
    name: "referral_code",
    placeholder: "Enter Referral Code",
    disabled: true,
  },
];
