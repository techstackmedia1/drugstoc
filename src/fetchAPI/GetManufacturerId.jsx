import { PRODUCT_DETAIL } from "../constants/_api.constant";
import { useConnect } from "../utilities/connectHooks";
import { useState, useEffect } from "react";
import axios from "axios";

const GetUserShippingAddress = () => {
  const { data } = useConnect({ path: PRODUCT_DETAIL });
  const [manufacturer, setManufacturer] = useState("");

  useEffect(() => {
    const fetchAPI = async () => {
      const res = await axios(data);
      const address = `${res.config.results[0].address_line1}, ${res.config.results[0].region}`;
      setManufacturer(address);
    };

    fetchAPI();
  }, [data]);

  return <span>{manufacturer}</span>;
};

export default GetUserShippingAddress;
