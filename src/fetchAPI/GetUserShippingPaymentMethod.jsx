import { WALLET_TRANSACTIONS } from "../constants/_api.constant";
import { useConnect } from "../utilities/connectHooks";
import { useState, useEffect } from "react";
import axios from "axios";

const GetUserShippingAddress = () => {
  const { data } = useConnect({ path: WALLET_TRANSACTIONS });
  const [shippingPaymentMethod, setShippingPaymentMethod] = useState("Select Your Payment method");

  useEffect(() => {
    const fetchAddress = async () => {
      const res = await axios(data, {
        headers: {
          Accept: "application/json",
        },
      });

      // setShippingPaymentMethod(res);
      console.log(res)
    };
    fetchAddress();
  }, [data]);

  return <span>{shippingPaymentMethod}</span>;
};

export default GetUserShippingAddress;

// https://staging.drugstoc.com/api/v2/inventory/products?category=&manufacturer=2261
