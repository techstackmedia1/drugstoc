import { BUSINESS_PROFILE } from "../constants/_api.constant";
import { useConnect } from "../utilities/connectHooks";
import { useState, useEffect } from "react";
import axios from "axios";

const GetUserPremisesPersonal = () => {
  const { data } = useConnect({ path: BUSINESS_PROFILE });
  const [userPremisesPersonal, setUserPremisesPersonal] = useState(null);

  useEffect(() => {
    const fetchAPI = async () => {
      const res = await axios(data, {
        headers: {
          Accept: "application/json",
        },
      });
      setUserPremisesPersonal(res.config.category);

    };
    fetchAPI();
  }, [data]);

  return <span>{userPremisesPersonal}</span>;
};

export default GetUserPremisesPersonal;
