import { PRODUCT_MANUFACTURERS } from "../constants/_api.constant";
import { useConnect } from "../utilities/connectHooks";
import { useState, useEffect } from "react";
import axios from "axios";

const GetManufacturerProducts = () => {
  const { data } = useConnect({ path: PRODUCT_MANUFACTURERS });
  const [manufacturerProducts, setManufacturerProducts] = useState(null);

  useEffect(() => {
    const fetchAddress = async () => {
      const res = await axios(data, {
        headers: {
          Accept: "application/json",
        },
      });

      console.log(res.config.results);
    };
    fetchAddress();
  }, [data]);

  return <span>{manufacturerProducts}</span>;
};

export default GetManufacturerProducts;
