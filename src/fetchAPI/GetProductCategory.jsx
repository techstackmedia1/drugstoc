import { PRODUCT_BY_CATEGORY } from "../constants/_api.constant";
import { useConnect } from "../utilities/connectHooks";
import { useState, useEffect } from "react";
import axios from "axios";

const GetProductCategory = () => {
  const { data } = useConnect({ path: PRODUCT_BY_CATEGORY });
  const [productCategory, setProductCategory] = useState(null);

  useEffect(() => {
    const fetchAddress = async () => {
      const res = await axios(data, {
        headers: {
          Accept: "application/json",
        },
      });

      //   console.log(res.config.results);
      console.log(res);
    };
    fetchAddress();
  }, [data]);

  return <span>{productCategory}</span>;
};

export default GetProductCategory;
