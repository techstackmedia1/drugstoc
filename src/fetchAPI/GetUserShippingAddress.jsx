import { SHIPPING_ADDRESS } from "../constants/_api.constant";
import { useConnect } from "../utilities/connectHooks";
import { useState, useEffect } from "react";
import axios from "axios";
import MultipleSelectPlaceholder from "../components/form.components";

const GetUserShippingAddress = () => {
  const { data } = useConnect({ path: SHIPPING_ADDRESS });
  const [shippingAddress, setShippingAddress] = useState(
    "Select Your shipping Address"
  );

  useEffect(() => {
    const fetchAddress = async () => {
      const res = await axios(data, {
        headers: {
          Accept: "application/json",
        },
      });
      // const address = `${res.config.results[0].address_line1}, ${res.config.results[0].region}`;
      const address = `${res.config.results[0].address_line1}`;
      setShippingAddress(address);
    };

    fetchAddress();
  }, [data]);

  return <span>{shippingAddress}</span>;
};

export default GetUserShippingAddress;
