import * as t from "../types";

const initialState = {
  cart: {
    count: 0,
    next: null,
    previous: null,
    results: []
  },
};

interface actionType {
    type: string,
    payload: any
}

export const shoppingReducer = (state = initialState, action: actionType) => {
  switch (action.type) {
    case t.FETCH_CART_PRODUCTS:
      return {
        ...state,
        cart: action.payload,
      };
    default:
      return state;
  }
};
