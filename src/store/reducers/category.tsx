import * as t from "../types";

let initialState = {
  category: {
    count: 0,
    next: null,
    previous: null,
    results: []
  },
  manufacturer: {
    count: 0,
    next: null,
    previous: null,
    results: []
  }
};

interface actionType {
    type: string,
    payload: any
}

export const categoryReducer = (state = initialState, action: actionType) => {
  switch (action.type) {
    case t.FETCH_PRODUCT_CATEGORY:
      return {
        ...state,
        category: action.payload,
      };
    case t.FETCH_PRODUCT_MANUFACTURER:
      return {
        ...state,
        manufacturer: action.payload,
      };
    default:
      return state;
  }
};
