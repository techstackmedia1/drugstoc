import * as t from "../types";

const initialState = {
  wallet: {
    count: 0,
    next: null,
    previous: null,
    results: []
  },
  transaction: {
    count: 0,
    next: null,
    previous: null,
    results: []
  } ,
};

interface actionType {
    type: string,
    payload: any
}

export const transactionReducer = (state = initialState, action: actionType) => {
  switch (action.type) {
    case t.FETCH_ACCOUNT_BALANCE:
      return {
        ...state,
        wallet: action.payload,
      };
    case t.FETCH_ACCOUNT_TRANSACTIONS:
      return {
        ...state,
        transaction: action.payload,
      };
    default:
      return state;
  }
};
