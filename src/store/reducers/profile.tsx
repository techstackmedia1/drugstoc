import * as t from "../types";

let initialState = {
    user: null
}

interface actionType {
    type: string,
    payload: any
}

export const userReducer = (state = initialState, action: actionType) => {
    switch(action.type) {
        case t.FETCH_USER_DATA:
            return {
                ...state, user: action.payload
            }
        default:
            return state;
    }
}