import * as t from "../types";

let initialState = {
  popular_products: {
    count: 0,
    next: null,
    previous: null,
    results: []
  },
  category_products: {
    count: 0,
    next: null,
    previous: null,
    results: []
  },
  search_result: {
    count: 0,
    next: null,
    previous: null,
    results: []
  }
};

interface actionType {
    type: string,
    payload: any
}

export const productsReducer = (state = initialState, action: actionType) => {
  switch (action.type) {
    case t.FETCH_POPULAR_PRODUCTS:
      return {
        ...state,
        popular_products: action.payload,
      };
    case t.FETCH_CATEGORY_PRODUCTS:
      return {
        ...state,
        category_products: action.payload,
      };
    case t.FETCH_PRODUCT_SEARCH_RESULT:
      return {
        ...state,
        search_result: action.payload,
      };
    default:
      return state;
  }
};
