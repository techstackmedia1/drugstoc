import * as t from "../types";

const initialState = {
  shipping: {
    count: 0,
    next: null,
    previous: null,
    results: []
  },
};

interface actionType {
    type: string,
    payload: any
}

export const shippingAddressReducer = (state = initialState, action: actionType) => {
  switch (action.type) {
    case t.FETCH_SHIPPING_ADDRESSES:
      return {
        ...state,
        shipping: action.payload,
      };
    default:
      return state;
  }
};
